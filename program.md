# Upravljanje Linux strežnikov

__***Avtor: Gregor Grešak***__

## Uvod: Linux

Linux je operacijski sistem, ki ga je med študijem leta 1991 začel razvijati Linus Torvalds in temelji na principih Unixa. Na področju osebnih računalnikov je Linux manj znan operacijski sistem, je pa zato toliko bolj popularen na področju strežnikov.

> Dviganje rok: 
  * kdo pozna Linux
  * kdo ga je že uporabljal
  * komu se je zdel težak za uporabo

Linux ima sloves operacijskega sistema, ki je zahteven za uporabnika, kar pa je le subjektivna ocena, saj je preprosto ponavadi tisto, na kar smo navajeni. Zame je naprimer francoščina povsem preprost jezik, marsikdo pa bo prisegel, da je težak. 

Ljudje, ki so navajeni predvsem na Operacijski sistem Windows se v Unixu podobnih sistemih ponavadi počutijo nekoliko izgubljeno. Tudi jaz sem se, a to je bilo pred davnimi sedmimi leti, ko sem se prvič s pomočjo ssh-ja prijavil v oddaljeni Linux strežnik. Čeprav sem moral opraviti le nekaj nastavitev, sem se odločil da pridem zadevi do dna in si namestil Linux na domačo "kišto". 

Z vami bi rad delil vse tisto, kar bi si takrat želel, da bi mi kdo razložil ... in še malo več. 

## Povezava: Putty

Če želite vspostaviti ssh (Secure SHell) povezavo s strežnikom, potrebujete ssh odjemalca. Standardni SSH odjemalec za Windows je Putty. Najdete ga lahko tukaj [http://www.putty.org/](http://www.putty.org/) Programček je mahjen in preprost za uporabo. Prepričam sem, da s tem ne boste imeli težav.

V Puttyjevo pogovorno okno vpišete IP strežnika in port. Privzet port za SSH je 22. Nastavite lahko tudi uporabniško ime sistem pa vas bo vprašal za geslo. Pošiljanju gesla se lahko izognete s tem, da v oddaljen računalnik vnesete svoj javni ključ (PUB key), če seveda imate na lokalnem računalniku generiran par ključev. Dobra praksa je, da na strežniku onemogočite prijavo z uporabniškim imenom in geslom in zahtevate izključno Public key authentication. Na ta način preprečite, da bi se v strežnik prijavil nekdo, ki nima certifikata (npr. z brute force ugibanjem gesla).

## Prvi koraki

Najprej poglejmo kje smo. Za to uporabimo ukaz ``pwd``, kar pomeni **p**rint **w**orking **d**irectory. Odgovor je ``/home/uporabnik/``. Edini ukaz, ki se ga spominjam iz DOS Terminala je ``dir``. Na unixu podobnih sistemih je to ukaz ``ls`` (**l**i**s**t), kar je primerno kratko. Trenuten direktorij je sicer vsaj na videz prazen. Za prikaz skritih datotek uporabimo ``ls -a``, za prikaz podrobnosti pa ``-l``, če to skombiniramo dobimo ``ls -al``. 

Pa pojdimo v bolj obljuden direktorij, med direktoriji se premikamo z ukazom ``cd`` (**c**hange **d**irectory). Če pot pričnemo s ``/`` pomeni, da imamo v mislih absolutno pot, sicer pa bo linux to razumel kot relativno pot. Pojdimo torej v direktorij ``cd /etc/`` in tukaj vtipkajmo ukaz ``ls -al``. Kot vidite je izpis zelo dolg. DOSov trik ``/p`` v linuxu ne deluje. To je prva izmed zagat, s katero se navadno srečajo uporabniki sistemov Windows. 

### less in pipe

Unixovi ukazi nimajo vgrajenega pagerja, obstaja pa ukaz, ki se imenuje ``less`` in ki opravlja funkcijo pagerja. Less vsak input izpiše po straneh. Za celo stran gor ali dol se pomikamo z PgUp in PgDn tipkami, za eno vrstico pa s puščicami. Za celo stran navzdol se premikamo s preslednico, za eno vrstico pa s tipko enter. Za izhod iz progama ``less`` uporabimo tipko ``q``. ``less dolgadatotoeka.txt`` omogoča branje datoteke, če pa želimo prebrati izpis ukaza ``ls -al`` moramo uporabiti pipeline. Izpis vseh datotek po straneh dobimo z ukazom ``ls -al | less`` S tem smo output prvega ukaza povezali z inputom drugega. Pipelini delujejo v unixu enako kot v DOSu.

## Kje je C:

Druga zagata je ponavadi razumevanje datotečni sistem oz. sistem direktorijev. V Windowsu so particije in/ali fizični diski ločeni s črkami C:, D: ... V Unix sistemih obstaja samo en korenski direkotrij in ta je ``/`` V njem pa se nahajajo standardni direktoriji:

```
/bin        izvedljive sistemske datoteke, ki se zaganjajo v zgodnji fazi zagona
/boot       datoteke bootloaderja
/dev        naprave in povezave do naprav, tukaj so tudi particije
/etc        sistemske nastavitvene datoteke
/home       domače mape uporabnikov
/lib        sistemske knjižnice
/media      mapa v katero se priklapljajo uporabniški datotečni sistemi
/mnt        mapa v katero se priklapljajo datotečni sistemi
/opt        neobvezni dodatki različnih programov - pogosto je prazen
/proc       informacije o procesih
/root       domača mapa korenskega uporabnika
/run        zgodnja priklopna točka za procese
/sbin       izvedljive datoteke, ki jih zaganja le superuser
/srv        podatki, ki so na voljo strežniškim aplikacijam 
/sys        podatki o sistemu
/tmp        začane datoteke. Ta direktorij se birše pri rebootu sistema
/usr        eden najpomembnejših direktorijev. Vsebuje večino binarnih datotek, knjižnic, priročniških strani, dokumentacije ... /usr ima standardno podstrukturo in je za procese "read only"
/var        zapisljiv sistem, v katerega procesi lahko vpisujejo podatke, 

```

Podstruktura ``/usr`` in ``/var``

```

/usr/bin        binarne datoteke na voljo celotnemu sistemu. **Tukaj iščeš ozvedljive programe!**
/usr/sbin       binarne datoteke, na volji celotnemu sistemu, ki zahtevajo root privilegije
/usr/lib        skupne programske knjižnice
/usr/doc        tekstovne datoteke z dokumentacijo o programih
/usr/local      lokalno kompilirani programi in pripadajoče knjižnice
/usr/share      različne datoteke, ki si jih lahko delijo vsi programi: npr. ikone, fonti ...
/usr/include    datoteke, ki se uporabljajo pri kompilaciji programov
/usr/src        datoteke z izvorno kodo in dokumentacijo kernela

/var/log        log datoteke
/var/lib        dinamične knjižnice
/var/mail       emaili
/var/spool      tiskalniške in druge vrste
/var/www        korenski direktorij spletnega strežnika

```

## Kje so izvedljive datoteke?

Ponavadi je ena od zadreg iskanje izvedljivih datotek, oz. programov. 

> Skoraj vse programe boste našli v /usr/bin ali /usr/local/bin

Za administratorje sta pomembna še direktorija ``/etc/, /var/``


## Končnice datotek

Verjetno se sprašujete, kar sem se tudi jaz: kako naj prepoznam izvedljive datoteke? Kakšna je njihova končnica?

V Linuxu končnice datotek nimajo nobenega posebnega pomena. V nekaterih primerih jih uporabljamo zato, da uporabnik razloči datoteke, ne pa sistem. Kateri program naj izvede datoteko ne izhaja iz njene končnice, pač pa iz prve vrstice t.i. shebang vrstice. Datoteke ``programcek.php`` sistem ne bo spoznal kot php datoteko, če se ne bo začela takole

```php
#!/usr/bin/php
<?php 

echo "Hello ...";

```

Zdaj lahko datoteko brez težav poimenujemo samo ``programcek`` ali celo ``programcek.py``. Sistema ne moti, da je končnica pythonova, bo pa zmotilo kolege administratorje. Datoteke znotraj direkotrijev ``/bin, /usr/bin ...`` so večinoma brez končnice.

Shebang pa ni dovolj, da bo datoteka samostojno izvedljiva. Imeti mora tudi t.i. executable bit ali dovoljenje za izvajanje. 

## Dovoljenja datotek v Linuxu

Dovoljenja datotek poznajo skoraj vsi, ki so kdaj uporabili kako php skripto na shared hostingu ali na lastnem Linux strežniku. Vsi vedo, da je potrebno "chmod" nastaviti na 777 za nekatere datoteke ali mape, redki pa v resnici vedo za kaj gre.

Vrnimo se v svojo domačo mapo. To storimo z ukazom ``cd`` brez dodatnih argumentov. Tukaj ustvarimo prazno datoteko. To storimo z ukazom ``touch primer.txt``. Poglejmo izpis ukaza ``ls -al``

```
drwxrwxr-x 2 gregor gregor 4096 apr 20 20:08 .
drwxrwxr-x 5 gregor gregor 4096 apr 20 20:07 ..
-rw-rw-r-- 1 gregor gregor    0 apr 20 20:08 primer.txt
```

Prvi stolpec vsebuje informacije o unix dovoljenjih, drugi stolpes podatek o številu hard linkov, ki kažejo na datoteko, tretji stolpec označuje lastnika datoteke, četrti skupino, ki ji je dodeljena datoteka, peti velikost datoteke v bytih, šesti datum in čas spremembe datoteke in zadnji ime datoteke.

V prvem stolpcu prva črka označuje, ali gre za direktorij ali za običajno datoteko, nato so tri črke, ki označujejo dovoljenja za lastnika, tri črke, ki dodeljujejo dovoljenja skupini in zadnje tri črke, ki dodeljujejo dovoljenja vsem ostalim.

Prvi dve vrstici sta linka. Prvi ``.`` označuje direktorij v katerem se nahajamo, drugi ``..`` pa njegov nadrejen direktorij. Relativno pot do naše datoteke laho napišemo tudi kot ``./primer.txt``

Naša datoteka ni izvedljiva saj v dovoljenjih nikjer ni črke ``x``

### chmod

Dovoljenja lahko nastavimo z oktalnimi števili (npr. 644, 755, 444 ...), ali pa opisno.

- u = user
- g = group
- o = others
- \+ = dodaj
- \- = odvzemi
- r = read
- w = write
- x = execute

Številke. Imamo natanko osem kombinacij
- 0 = ---
- 1 = --x
- 2 = -w-
- 3 = -wx
- 4 = r--
- 5 = r-x
- 6 = rw-
- 7 = rwx

Primeri

```
$ chmod 777 primer.txt
$ ls -al primer.txt
-rwxrwxrwx 1 gregor gregor    0 apr 20 20:08 primer.txt
$ chmod o-x primer.txt
$ ls -al primer.txt
-rwxrwxrw- 1 gregor gregor    0 apr 20 20:08 primer.txt
$ chmod g-w, o-w primer.txt
$ ls -al primer.txt
-rwxr-xr-- 1 gregor gregor    0 apr 20 20:08 primer.txt

```
itd.

Rekurzivno spreminjamo dovoljenja skozi direktorijsko struktiro z opcijo -R ``chmod -R 755 mapa/``

### chown

Seveda pa je odvisno, katera skupina in uporabnik sta lastnika datoteke. Lastnike spreminjamo z ukazom chown. Tudi chown ima opcijo -R za rekurzivno spreminjanje lastništva.

```
$ chown root:sudo primer.txt
Operation not permited.
$ sudo chown root:sudo primer.txt
Sudo password:
$
```
Običajen lastnik ne more dodeljevati lastništva, le superuser lahko to počne. Zato uporabimo sudo

## Sudo ... su-what?

sudo pomeni superuser do.

V Linuxu se superuser imenuje root. Zaradi varnosti ima root pogosto onemogočen login. To je privzeta nastavitev na Ubuntuju. Uporabnika sicer spreminjamo z ukazom su (**s**wich **u**ser), toda če napišemo ``su root``, nas bo sistem vprašal za geslo, ki pa za uporabnika root sploh ne obstaja.

Ker večino operacij lahko delamo kot običajni uporabnik, in v izogib zagatam z lastništvom datotek (privzeti lastnik datoteke je tisti, ki jo ustvari), delamo vse kar lahko kot navaden uporabnik. Kadar potrebujemo superuser pravice uporabimo sudo, ki nam samo za tisti ukaz podeli superuser pravice (seveda v zameno za geslo, ki pa je naše uporabniško geslo). Sistem vpraša za geslo le enkrat, nato pa znova le če sudo ni bil uporabljen v zadnjih petih minutah.

Prednost sudo-ja je tudi, da sistem v log datoteke vpiše uporabnika, ki je sudo uporabil, kar je še posebej dobro, kadar želimo ugotoviti, kdo je pokvaril sistem.

Da bi sudo deloval, mora biti uporabnik vpisan v datoteko ``/etc/sudoers`` K sreči nam ni potrebno te datoteke neposredno spreminjati saj za to obstaja ukaz ``sudo adduser uporabnik sudo``

Včasih, še posebno kadar urejamo več konfiguracijskih datotek in se ukvarjamo res samo z visokimi sistemskimi zadevami, si kljub vsemu želimo postati root za dalj časa. To dosežemo z ukazom ``sudo -i`` (i kot interactive)

## Nov uporabnik, sprememba gesla

Kot sudo lahko kreiramo nove uporabnike.
```
$ sudo useradd novuporabnik
```
Z ukatom ``useradd -D`` izpišemo privzete nastavitve za novega uporabnika. Slednje lahko spremenimo med kreacijo z opcijami.

Svoje geslo spremenimo z ukazom ``passwd``, geslo drugemu uporabniku pa z ``sudo passwd uporabnik``

Če želimo dodati novo skupino lahko to naredimo z ukazom ``groupadd [opcije] imeskupine``

Uporabnika odstranimo z ``userdel ime``. Z opcijo ``-r`` zbrišemo tudi njegovo domačo mapo. Po istem sistemu deluje tudi ``groupdel``

Če pa želimo uporabnika odstraniti iz skupine uporabimo ``id`` da ugotovimo v katerih skupinah je uporabnik in nato z ``usermod -G`` določimo le skupine v katerih naj ostane.


## Koliko prostora je še prostega v datotečnem sistemu

Naslednja od zagat je preverjanje prostora na datotečnem sistemu. Osnovne informacije o diskih in particijah ``sudo fdisk -l`` Diski so ponavadi označeni kot sda, sdb ..., particije pa sda1, sda2 ... Informacije o priklopnih točkag particij se nahajajo v datoteki ``/etc/fstab`` Vsaj ena particija, ponavadi sda1 je priklopljena na korenski direktorij / 

Ukaz za preverjanje uporabe diska se imenuje ``du`` (**d**isk **u**sage). Opcija ``-h`` izpiše rezultate v bolj berljivi obliki, dati pa mu moramo še pot do direktorija, katerega porabo želimo poznati ``du -h /home``. Ukaz deluje rekurzivno, zato je lahko izpis precej dolg. 

V resnici pa nezaseden prostor na disku preverjamo z ukazom ``df -h`` (**d**isk **f**ree). Ta ukaz prikaže podatke o skupnem, prostem in zasedenem prostoru za vse priklopljene datotečne sisteme.  


## Ctrl+Alt+del = top

Informacije o procesih, top, htop, nmon, ps, uptime in seveda kill, killall

## Inštalacija programskih paketov

apt-get apt-cache

## Linki

V Linuxu so datoteke pointerji na inode. T.i. hard linki so aliasi za iste inode. Na ta način imamo lahko eno datoteko na več mestih, ne da bi porabila več prostora, kot ena datoteka. 

Softlinki kažejo na datoteko in se zelo pogosto uporabljajo. Link ustvarimo z ukazom ``ln`` Naredimo link datoteke ``primer.txt`` v nov direktorij ``test/`` Pri izdelavi linkov vedno uporabljamo absolutno pot sicer se pri premiku linka, slednji lahko pokvari.

```
$ mkdir test
$ ls 
primer.txt test
$ ln -s /home/user/primer.txt test/
$ ls -al test/
lrwxrwxrwx 1 gregor gregor   32 apr 20 21:57 primer.txt -> /home/gregor/primer.txt
```
Prva črka v prvem stolpcu je ``l``, kar pomeni, da ne gre za navadno datoteko. V zadnjem stolpcu je jasno navedeno, kam link kaže.

## Kaj pomeni rm -rf in zakaj je to strašno

cp, mv, rm, rmdir in tudi mkdir

Rekurzivno brisanje direktorijav rm -rf Uporabljaj zelo previdno!


## grep     

Ukaz grep je zelo primeren za iskanje vsebine po datotekah

## find

Ukaz find je priročen za iskanje datotek po imenih. Imenitna sta tudi whereis in locate, še posebno za iskanje programov.

## Crontab - periodično izvajanje opravil

Če bo čas ...

___

### PUB Key Authentication

Najprej v oddaljeni računalnik skopiramo svoj javni ključ. Za generiranje ključev lahko uporabite puttygen.exe, za kopiranje v oddaljeni računalnik pa pscp.exe Ključ moramo dodati v datoteko authorized_keys. To naredimo z naslednjim ukazom

``
cat moj_kljuc.pub >> ~/.ssh/authorized_keys
``

Če authorized keys ne obstaja, bo s tem ukazom ustvarjena.

Preden naredimo karkoli drugega, je dobro da preizkusimo delovanje ključa, sicer se nam lahko zgodi, da se zaklenemo ven. Torej se odjavimo iz sistema in znova prijavimo preko puttyja, tokrat brez uporabniškega imena in gesla. Če je ključ pravilno nameščen, mora to delovati, v nasprotnem primeru se znova prijavimo z uporabniškim imenom in geslom, ter stvar uredimo. Ko enkrat deluje, nadaljujemo. 

Na oddaljenem računalniku odpremo nastavitveno datoteko za sshd. Ta se nahaja v `` /etc/ssh/sshd_config `` Najdemo in spremenimo sledeče vrstice:

```
PasswordAuthentication no

RSAAuthentication yes
PubkeyAuthentication yes
```

Nastavitve so lahko tudi bolj specifične, npr. samo za določene uporabnike ali skupine uporabnikov. Več o tem tukaj http://www.linux.org/threads/how-to-force-ssh-login-via-public-key-authentication.4253/

Ko končamo z urejanjem nastavitvene datoteke, moramo narediti reload ssh serverja:

``
sudo service ssh reload
``
# Test

<!--stackedit_data:
eyJoaXN0b3J5IjpbMTY3MTMyOTgxOCwxNDAzNzE4ODc1XX0=
-->